//  class animal
public class Mobil {
        
        // Properti (variabel) dari kelas Mobil
        String warna;
        String no_Plat;

        // String warna;
        // String no_Plat;

        // // Metode info() untuk menampilkan informasi mobil
        void info() {
            System.out.println("Warna: " + warna);
            System.out.println("Nomor Plat: " + no_Plat);
        }
        // void info() {
        //     System.out.println("Warna: " + warna);
        //     System.out.println("Nomor Plat: " + no_Plat);
        // }

        public static void main(String args[]) {
            System.out.println("Mobil 1");
            // Membuat objek m dari kelas Mobil
            Mobil m = new Mobil();

            // Mengatur properti objek m
            m.warna = "hitam";
            m.no_Plat = "KT 2837 UE";

            // Memanggil metode info() untuk menampilkan informasi mobil
            m.info();
        }
    }