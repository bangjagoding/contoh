public class testTabungan {
    public static void main(String args[]) {
        // Membuat objek Tabungan dengan saldo awal 5000
        Tabungan t = new Tabungan(5000);
        
        System.out.println("Saldo awal Tabungan Anda : " + t.saldo);
        
        // Mengambil uang sebesar 1500 dari tabungan
        t.ambilUang(1500);
        System.out.println("Jumlah uang yang diambil: 1500");
        
        // Menampilkan saldo tabungan setelah pengambilan
        System.out.println("Saldo Tabungan Anda sekarang adalah: " + t.saldo);
    }
}

// class Tabungan {
//     int saldo;

//     public Tabungan(int saldoAwal) {
//         saldo = saldoAwal;
//     }

//     public void ambilUang(int jumlah) {
//         if (jumlah <= saldo) {
//             saldo -= jumlah;
//         } else {
//             System.out.println("Saldo tidak mencukupi.");
//         }
//     }
// }
