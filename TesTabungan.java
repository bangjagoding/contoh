public class TesTabungan {
    public static void main(String args[]) {
        boolean status;
        Tabungan tabungan = new Tabungan(10000);
        System.out.println("Saldo awal: " + tabungan.getSaldo());
        
        // Menyimpan 8000 ke dalam tabungan
        tabungan.simpanUang(8000);
        System.out.println("Jumlah uang yang disimpan: 8000");
        
        // Mengambil 7000 dari tabungan dan menyimpan status pengambilan
        status = tabungan.ambilUang(7000);
        System.out.print("Jumlah uang yang diambil: 7000 ");
        if (status) {
            System.out.println("ok");
        } else {
            System.out.println("gagal");
        }
        
        // Menyimpan 1000 ke dalam tabungan
        tabungan.simpanUang(1000);
        System.out.println("Jumlah uang yang disimpan: 1000");
        
        // Mengambil 10000 dari tabungan dan menyimpan status pengambilan
        status = tabungan.ambilUang(10000);
        System.out.print("Jumlah uang yang diambil: 10000 ");
        if (status) {
            System.out.println("ok");
        } else {
            System.out.println("gagal");
        }
        
        // Mengambil 2500 dari tabungan dan menyimpan status pengambilan
        status = tabungan.ambilUang(2500);
        System.out.print("Jumlah uang yang diambil: 2500 ");
        if (status) {
            System.out.println("ok");
        } else {
            System.out.println("gagal");
        }
        
        // Menyimpan 2000 ke dalam tabungan
        tabungan.simpanUang(2000);
        System.out.println("Jumlah uang yang disimpan: 2000");
        System.out.println("Saldo sekarang = " + tabungan.getSaldo());
    }
}

class Tabungan {
    int saldo;

    public Tabungan(int saldoAwal) {
        saldo = saldoAwal;
    }

    public void simpanUang(int jumlah) {
        saldo += jumlah;
    }

    public boolean ambilUang(int jumlah) {
        if (saldo >= jumlah) {
            saldo -= jumlah;
            return true;
        } else {
            return false;
        }
    }

    public int getSaldo() {
        return saldo;
    }
}

